import { defineStore } from "pinia";
import { ref } from "vue";

export const useRecipeStore = defineStore("recipe", () => {
  const title = ref("Simple Omelette Recipe");
  const foodImage = ref("image-omelette");
  const description = ref(
    "An easy and quick dish, perfect for any meal. This classic omelette combines beaten eggs cooked to perfection, optionally filled with your choice of cheese, vegetables, or meats."
  );
  const preparationTime = ref([
    {
      title: "Total",
      description: "Approximately 10 minutes",
    },
    {
      title: "Preparation",
      description: "5 minutes",
    },
    {
      title: "Cooking",
      description: "5 minutes",
    },
  ]);
  const ingredients = ref([
    "2-3 large eggs",
    "Salt, to taste",
    "Pepper, to taste",
    "1 tablespoonof butter or oil",
    "Optional fillings: cheese, diced vegetables, cooked meat, herbs",
  ]);
  const instructions = ref([
    {
      title: "Beat the eggs",
      description:
        "In a bowl, beat the eggs with a pinch of salt and pepper until they are well mixed. You can add a tablespoon of water or milk for a fluffier texture.",
    },
    {
      title: "Heat the pan",
      description:
        "Place a non-stick frying pan over medium heat and add butter or oil.",
    },
    {
      title: "Cook the omelette",
      description:
        "Once the butter is melted and bubbling, pour in the eggs. Tilt the pan to ensure the eggs evenly coat the surface.",
    },
    {
      title: "Add fillings (optional)",
      description:
        "When the eggs begin to set at the egdes, but are still slightly runny in the middle, sprinkle your chosen fillings over one half of the omelette.",
    },
    {
      title: "Fold and serve",
      description:
        "As the omelette continues to cook, carefully lift one edge and fold it over the fillings. Let it cook for another minute, then slide it onto a plate.",
    },
    {
      title: "Enjoy",
      description: "Serve hot, with additional salt and pepper if needed.",
    },
  ]);
  const nutritionList = ref([
    {
      title: "Calories",
      value: 277,
      unit: "kcal",
    },
    {
      title: "Carbs",
      value: 0,
      unit: "g",
    },
    {
      title: "Protein",
      value: 20,
      unit: "g",
    },
    {
      title: "Fat",
      value: 22,
      unit: "g",
    },
  ]);
  const nutritionOverview = ref(
    "The table below shows the nutritional values per serving without the additional fillings."
  );

  return {
    title,
    description,
    preparationTime,
    ingredients,
    instructions,
    foodImage,
    nutritionList,
    nutritionOverview,
  };
});
